module Lib
    ( startApp
    ) where

import qualified Model.Genres            as Genres
import qualified Scraper.Metadata.BDGest as BDGest


startApp :: IO ()
startApp = do
    putStrLn $ show $ Genres.add (Genres.genre "Historical") Genres.empty
    BDGest.album
