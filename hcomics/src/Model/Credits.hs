{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Model.Credits
  ( Credit (..)
  , colorist
  , coverArtist
  , design
  , editor
  , foreword
  , inker
  , letterer
  , other
  , penciller
  , scenery
  , storyboard
  , translator
  , writer
  , Credits (..)
  , empty
  , with
  , add
  ) where

import           GHC.Generics

import qualified Model.Person as Person
import qualified Model.Role   as Role


data Credit
  = Credit { person     :: Person.Person
           , role       :: Role.Role
           , is_primary :: Bool
           }
  deriving (Show, Read, Generic, Eq)

colorist :: Person.Person -> Credit
colorist person = Credit { person = person, role = Role.colorist, is_primary = False }

coverArtist :: Person.Person -> Credit
coverArtist person = Credit { person = person, role = Role.coverArtist, is_primary = False }

design :: Person.Person -> Credit
design person = Credit { person = person, role = Role.design, is_primary = False }

editor :: Person.Person -> Credit
editor person = Credit { person = person, role = Role.editor, is_primary = False }

foreword :: Person.Person -> Credit
foreword person = Credit { person = person, role = Role.foreword, is_primary = False }

inker :: Person.Person -> Credit
inker person = Credit { person = person, role = Role.inker, is_primary = False }

letterer :: Person.Person -> Credit
letterer person = Credit { person = person, role = Role.letterer, is_primary = False }

other :: Person.Person -> Credit
other person = Credit { person = person, role = Role.other, is_primary = False }

penciller :: Person.Person -> Credit
penciller person = Credit { person = person, role = Role.penciller, is_primary = False }

scenery :: Person.Person -> Credit
scenery person = Credit { person = person, role = Role.scenery, is_primary = False }

storyboard :: Person.Person -> Credit
storyboard person = Credit { person = person, role = Role.storyboard, is_primary = False }

translator :: Person.Person -> Credit
translator person = Credit { person = person, role = Role.translator, is_primary = False }

writer :: Person.Person -> Credit
writer person = Credit { person = person, role = Role.writer, is_primary = False }


newtype Credits = Credits
    { content :: [Credit]
    } deriving (Show, Read, Generic, Eq)


empty :: Credits
empty = Credits {content = []}

with :: [Credit] -> Credits
with values = Credits {content = values}

add :: Credit -> Credits -> Credits
add credit credits =
    credits { content = content credits ++ [credit] }
