{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Model.Genres
  ( Genre (..)
  , genre
  , Genres(..)
  , empty
  , with
  , add
  ) where

import           GHC.Generics

newtype Genre = Genre
    { value :: String
    } deriving (Show, Read, Generic, Eq, Ord)

genre :: String -> Genre
genre name = Genre {value = name}


newtype Genres = Genres
    { content :: [Genre]
    } deriving (Show, Read, Generic, Eq)

empty :: Genres
empty = Genres {content = []}

with :: [Genre] -> Genres
with values = Genres {content = values}

add :: Genre -> Genres -> Genres
add genre genres =
    genres { content = content genres ++ [genre] }
