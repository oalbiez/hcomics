{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Model.IssueIndex
  ( IssueIndex (..)
  , none
  ) where

import           GHC.Generics


data IssueIndex
  = IssueIndex { issue :: Maybe String
               , count :: Maybe String
               }
  deriving (Show, Read, Generic, Eq)

none :: IssueIndex
none = IssueIndex { issue = Nothing, count = Nothing}
