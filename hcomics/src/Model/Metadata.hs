{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Model.Metadata
  ( Metadata (..)
  , setTitle
  ) where

import           GHC.Generics

import qualified Data.Time.Calendar as Calendar
import qualified Model.Credits      as Credits
import qualified Model.Genres       as Genres
import qualified Model.IssueIndex   as IssueIndex
import qualified Model.Pages        as Pages
import qualified Model.Publisher    as Publisher
import qualified Model.Series       as Series
import qualified Model.Tags         as Tags
import qualified Model.Title        as Title
import qualified Model.VolumeIndex  as VolumeIndex


data Plot
  = Plot { summary                :: String
         , characters             :: String
         , teams                  :: String
         , locations              :: String
         , main_character_or_team :: String
         }
  deriving (Show, Read, Generic, Eq)


data Metadata
  = Metadata { series    :: Series.Series
             , title     :: Title.Title
             , issue     :: IssueIndex.IssueIndex
             , volume    :: VolumeIndex.VolumeIndex
             , date      :: Calendar.Day
             , publisher :: Publisher.Publisher
             , genres    :: Genres.Genres
             , plot      :: Plot
             , credits   :: Credits.Credits
             , tags      :: Tags.Tags
             , pages     :: Pages.Pages
             }
  deriving (Show, Read, Generic)

setTitle :: Title.Title -> Metadata -> Metadata
setTitle title metadata = metadata { title = title }
