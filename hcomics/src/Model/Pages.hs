{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Model.Pages
  ( Page (..)
  , Pages (..)
  , empty
  , with
  , add
  ) where

import           GHC.Generics


data Page
  = Page { kind         :: String
         , bookmark     :: String
         , double_page  :: Bool
         , image        :: Int
         , image_size   :: String
         , image_height :: String
         , image_width  :: String
         }
  deriving (Show, Read, Generic, Eq)


newtype Pages = Pages
    { content :: [Page]
    } deriving (Show, Read, Generic, Eq)


empty :: Pages
empty = Pages {content = []}

with :: [Page] -> Pages
with values = Pages {content = values}

add :: Page -> Pages -> Pages
add credit credits =
    credits { content = content credits ++ [credit] }
