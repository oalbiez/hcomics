{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Model.Person
  ( Person (..)
  ) where

import           GHC.Generics


newtype Person = Person
  { name :: String
  } deriving (Show, Read, Generic, Eq, Ord)

