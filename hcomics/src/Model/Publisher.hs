{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Model.Publisher
  ( Publisher (..)
  ) where

import           GHC.Generics

newtype Publisher = Publisher
  { value :: String
  } deriving (Show, Read, Generic, Eq, Ord)

