{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Model.Role
  ( Role (..)
  , role
  , colorist
  , coverArtist
  , design
  , editor
  , foreword
  , inker
  , letterer
  , other
  , penciller
  , scenery
  , storyboard
  , translator
  , writer
  ) where

import           GHC.Generics


newtype Role = Role
  { name :: String
  } deriving (Show, Read, Generic, Eq, Ord)

role :: String -> Role
role name = Role {name = name }

colorist :: Role
colorist = role "Colorist"

coverArtist :: Role
coverArtist = role "CoverArtist"

design :: Role
design = role "Design"

editor :: Role
editor = role "Editor"

foreword :: Role
foreword = role "Foreword"

inker :: Role
inker = role "Inker"

letterer :: Role
letterer = role "Letterer"

other :: Role
other = role "Other"

penciller :: Role
penciller = role "Penciller"

scenery :: Role
scenery = role "Scenery"

storyboard :: Role
storyboard = role "Storyboard"

translator :: Role
translator = role "Translator"

writer :: Role
writer = role "Writer"
