{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Model.Series
  ( Series (..)
  ) where

import           GHC.Generics


newtype Series = Series
  { value :: String
  } deriving (Show, Read, Generic, Eq, Ord)

