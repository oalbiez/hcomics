{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Model.Tags
  ( Tag (..)
  , tag
  , Tags(..)
  , empty
  , with
  , add
  ) where

import           GHC.Generics

newtype Tag = Tag
    { value :: String
    } deriving (Show, Read, Generic, Eq, Ord)

tag :: String -> Tag
tag name = Tag {value = name}


newtype Tags = Tags
    { content :: [Tag]
    } deriving (Show, Read, Generic, Eq)

empty :: Tags
empty = Tags {content = []}

with :: [Tag] -> Tags
with values = Tags {content = values}

add :: Tag -> Tags -> Tags
add tag tags =
    tags { content = content tags ++ [tag] }
