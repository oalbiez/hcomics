{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Model.Title
  ( Title (..)
  ) where

import           GHC.Generics


newtype Title = Title
  { value :: String
  } deriving (Show, Read, Generic, Eq, Ord)

