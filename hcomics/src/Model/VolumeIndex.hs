{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Model.VolumeIndex
  ( VolumeIndex (..)
  , none
  ) where

import           GHC.Generics


data VolumeIndex
  = VolumeIndex { issue :: Maybe Int
                , count :: Maybe Int
                }
  deriving (Show, Read, Generic, Eq)

none :: VolumeIndex
none = VolumeIndex { issue = Nothing, count = Nothing}
