{-# LANGUAGE DataKinds     #-}
{-# LANGUAGE DeriveGeneric #-}

module Scraper.Metadata.BDGest
  ( album
  ) where

import qualified Model.Metadata         as Metadata
import           Text.HTML.Scalpel
import           Text.HTML.TagSoup
import           Text.HTML.TagSoup.Tree

album :: IO ()
album = do
    contents <- readFile "../data/BD-Thorgal-Tome-1-La-Magicienne-trahie-398.html"
    let root = parseTree contents

    let divs = [x | x@(TagBranch "div" _ _) <- universeTree root]
    -- content = soup.find("div", attrs={"data-role": "content"})

    i <- scrapeStringLike contents ("div" @: [(AttributeString "data-role") @= "content"])$ do
        index   <- position
        return index

    putStrLn $ show i

    --putStrLn $ show divs
    putStrLn "Done."
